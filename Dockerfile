FROM solr:5.3

ADD mecab-ko-solr-plugin.tgz /

USER root

RUN ldconfig && \ 
    cd /opt/solr/server/solr/configsets/data_driven_schema_configs/conf/ && \
    mv managed-schema managed-schema.orig && \
    mv managed-schema.new managed-schema && \
    mv solrconfig.xml solrconfig.xml.orig && \
    mv solrconfig.xml.new solrconfig.xml && \
    chown -R $SOLR_USER:$SOLR_USER /opt/solr

USER $SOLR_USER

ENV SOLR_OPTS -Djava.library.path=/usr/local/lib

CMD ["solr"]
